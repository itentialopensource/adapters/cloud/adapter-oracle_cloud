## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Oracle Cloud. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Oracle Cloud.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Oracle Cloud. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12SharesFolderId(folderId, payload, callback)</td>
    <td style="padding:15px">Share Folder or Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/shares/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDocumentsApi12SharesFolderId(folderId, payload, callback)</td>
    <td style="padding:15px">Unshare Folder</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/shares/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12SharesFolderIdItems(folderId, payload, callback)</td>
    <td style="padding:15px">Get Shared Folder Users</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/shares/{pathv1}/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDocumentsApi12SharesFolderIdRole(folderId, payload, callback)</td>
    <td style="padding:15px">Edit Shared Folder User Role</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/shares/{pathv1}/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDocumentsApi12SharesFolderIdUser(folderId, payload, callback)</td>
    <td style="padding:15px">Revoke User</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/shares/{pathv1}/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDocumentsApi12SharesFolderIdMyuser(folderId, callback)</td>
    <td style="padding:15px">Revoke Current User</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/shares/{pathv1}/myuser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12MetadataCollectionName(collectionName, payload, callback)</td>
    <td style="padding:15px">Create Metadata Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/metadata/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDocumentsApi12MetadataCollectionName(collectionName, payload, callback)</td>
    <td style="padding:15px">Edit Metadata Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/metadata/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12MetadataCollectionName(collectionName, callback)</td>
    <td style="padding:15px">Get Metadata Collection Definition</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/metadata/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDocumentsApi12MetadataCollectionName(collectionName, callback)</td>
    <td style="padding:15px">Delete Metadata Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/metadata/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDocumentsApi12MetadataCollectionNameField(collectionName, payload, callback)</td>
    <td style="padding:15px">Edit Fields in a Metadata Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/metadata/{pathv1}/field?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12MetadataSearchFields(callback)</td>
    <td style="padding:15px">Get Searchable Metadata Fields</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/metadata/searchFields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12MetadataSearchFields(payload, callback)</td>
    <td style="padding:15px">Set Searchable Metadata Fields</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/metadata/searchFields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12Metadata(payload, callback)</td>
    <td style="padding:15px">Get Available Metadata Collections</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FoldersFolderIdMetadataCollectionName(folderId, collectionName, callback)</td>
    <td style="padding:15px">Assign a Metadata Collection to a Folder</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/metadata/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FoldersFolderIdMetadata(folderId, payload, callback)</td>
    <td style="padding:15px">Assign Values to a Folder Metadata Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FoldersFolderIdMetadata(folderId, directAssigned, callback)</td>
    <td style="padding:15px">Get Folder Metadata Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDocumentsApi12FoldersFolderIdMetadata(folderId, payload, callback)</td>
    <td style="padding:15px">Delete Values in a Folder Metadata Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FoldersFolderIdMetadataFields(folderId, directAssigned, callback)</td>
    <td style="padding:15px">Get Folder Assigned Metadata Collections</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/metadataFields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FoldersFolderIdBulkCreate(folderId, payload, links, callback)</td>
    <td style="padding:15px">Bulk Create Folders</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/_bulkCreate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FoldersFolderIdDownload(folderId, payload, callback)</td>
    <td style="padding:15px">Bulk Download Folders and Files</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/_download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FoldersFolderIdDownloadJobId(folderId, jobId, callback)</td>
    <td style="padding:15px">Get Status of a Bulk Download Job</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/_download/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDocumentsApi12FoldersFolderIdDownloadJobId(folderId, jobId, callback)</td>
    <td style="padding:15px">Abort a Bulk Download Job</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/_download/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FoldersFolderIdDownloadJobIdPackage(folderId, jobId, callback)</td>
    <td style="padding:15px">Download a Bulk Download Job File</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/_download/{pathv2}/package?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FoldersFolderId(folderId, callback)</td>
    <td style="padding:15px">Get Folder</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FoldersFolderId(folderId, payload, callback)</td>
    <td style="padding:15px">Create Folder</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDocumentsApi12FoldersFolderId(folderId, payload, callback)</td>
    <td style="padding:15px">Edit Folder</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDocumentsApi12FoldersFolderId(folderId, callback)</td>
    <td style="padding:15px">Delete Folder</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FoldersFolderIdCopy(folderId, payload, callback)</td>
    <td style="padding:15px">Copy Folder</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/copy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FoldersFolderIdMove(folderId, payload, callback)</td>
    <td style="padding:15px">Move Folder</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FoldersItems(orderby, limit, offset, callback)</td>
    <td style="padding:15px">Get Home Folder Contents</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FoldersFolderIdItems(folderId, filterName, orderby, limit, offset, callback)</td>
    <td style="padding:15px">Get Specific Folder Contents</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FoldersSearchItems(fulltext, querytext, orderby, limit, offset, fields, callback)</td>
    <td style="padding:15px">Search Folders or Files</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/search/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FoldersFolderIdSearchItems(folderId, fulltext, querytext, orderby, limit, offset, fields, callback)</td>
    <td style="padding:15px">Search Folders or Files Under Specific Folder ID</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/search/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FoldersLists(idList, checkStatus, callback)</td>
    <td style="padding:15px">Get Information on Multiple Folders</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FoldersFolderIdTags(folderId, directAssigned, callback)</td>
    <td style="padding:15px">Get Folder Tags</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FoldersFolderIdTags(folderId, payload, callback)</td>
    <td style="padding:15px">Set Folder Tags</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDocumentsApi12FoldersFolderIdTags(folderId, payload, callback)</td>
    <td style="padding:15px">Edit Folder Tags</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDocumentsApi12FoldersFolderIdTags(folderId, callback)</td>
    <td style="padding:15px">Delete All Folder Tags</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FoldersFolderIdConversation(folderId, payload, callback)</td>
    <td style="padding:15px">Create Folder Conversation</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/folders/{pathv1}/conversation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FilesData(jsonInputParameters, primaryFile, metadataValues, callback)</td>
    <td style="padding:15px">Upload File</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FilesFileIdData(fileId, primaryFile, metadataValues, callback)</td>
    <td style="padding:15px">Upload File Version</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesFileIdData(fileId, version, callback)</td>
    <td style="padding:15px">Download File</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FilesFileIdMetadataCollectionName(fileId, collectionName, callback)</td>
    <td style="padding:15px">Assign a Metadata Collection to a File</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/metadata/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FilesFileIdMetadata(fileId, payload, callback)</td>
    <td style="padding:15px">Assign Values to a File Metadata Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesFileIdMetadata(fileId, directAssigned, callback)</td>
    <td style="padding:15px">Get File Metadata Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDocumentsApi12FilesFileIdMetadata(fileId, payload, callback)</td>
    <td style="padding:15px">Delete Values in File Metadata Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesFileIdMetadataFields(fileId, directAssigned, callback)</td>
    <td style="padding:15px">Get File Assigned Metadata Collections</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/metadataFields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesLists(idList, callback)</td>
    <td style="padding:15px">Get Information on Multiple Files</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesFileId(fileId, version, includeRenditions, includeOnlyExistingRenditions, callback)</td>
    <td style="padding:15px">Get File Information</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDocumentsApi12FilesFileId(fileId, payload, callback)</td>
    <td style="padding:15px">Edit File</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDocumentsApi12FilesFileId(fileId, version, callback)</td>
    <td style="padding:15px">Delete File</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FilesFileIdCopy(fileId, payload, callback)</td>
    <td style="padding:15px">Copy File</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/copy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FilesFileIdMove(fileId, payload, callback)</td>
    <td style="padding:15px">Move File</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesFileIdVersions(fileId, callback)</td>
    <td style="padding:15px">Get File Versions</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FilesFileIdReserve(fileId, callback)</td>
    <td style="padding:15px">Reserve File</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/reserve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FilesFileIdUnreserve(fileId, payload, callback)</td>
    <td style="padding:15px">Unreserve File</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/unreserve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesFileIdDataThumbnailImage(fileId, format, version, callback)</td>
    <td style="padding:15px">Get Responsive Thumbnail Image</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/data/thumbnailImage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesFileIdDataSmallImage(fileId, format, version, callback)</td>
    <td style="padding:15px">Get Responsive Small Image</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/data/smallImage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesFileIdDataMediumImage(fileId, format, version, callback)</td>
    <td style="padding:15px">Get Responsive Medium Image</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/data/mediumImage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesFileIdDataLargeImage(fileId, format, version, callback)</td>
    <td style="padding:15px">Get Responsive Large Image</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/data/largeImage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesFileIdDataThumbnail(fileId, version, callback)</td>
    <td style="padding:15px">Get Thumbnail</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/data/thumbnail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesFileIdDataRendition(fileId, rendition, version, renditionType, callback)</td>
    <td style="padding:15px">Get Rendition</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/data/rendition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FilesFileIdDataRendition(fileId, jsonInputParameters, file, callback)</td>
    <td style="padding:15px">Upload Custom Rendition</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/data/rendition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDocumentsApi12FilesFileIdDataRendition(fileId, rendition, version, callback)</td>
    <td style="padding:15px">Delete Custom Rendition</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/data/rendition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesFileIdDataRenditions(fileId, renditionType, version, callback)</td>
    <td style="padding:15px">List Renditions</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/data/renditions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesFileIdPages(fileId, version, callback)</td>
    <td style="padding:15px">Get Rendition Page Count</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/pages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FilesFileIdPages(fileId, version, callback)</td>
    <td style="padding:15px">Generate File Renditions</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/pages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesFileIdPreviewPath(fileId, version, waitForCompletePreview, callback)</td>
    <td style="padding:15px">Get File HTML5 Preview</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/previewPath?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesFileIdTags(fileId, directAssigned, callback)</td>
    <td style="padding:15px">Get File Tags</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FilesFileIdTags(fileId, payload, callback)</td>
    <td style="padding:15px">Set File Tags</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDocumentsApi12FilesFileIdTags(fileId, payload, callback)</td>
    <td style="padding:15px">Edit File Tags</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDocumentsApi12FilesFileIdTags(fileId, callback)</td>
    <td style="padding:15px">Delete All File Tags</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12FilesFileIdConversation(fileId, payload, callback)</td>
    <td style="padding:15px">Create File Conversation</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/conversation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12FilesFileIdAccesses(fileId, limit, offset, orderBy, callback)</td>
    <td style="padding:15px">Get File Accesses</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/files/{pathv1}/accesses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12PubliclinksFolderFolderId(folderId, payload, callback)</td>
    <td style="padding:15px">Create Folder Public Link</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/publiclinks/folder/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12PubliclinksFolderFolderId(folderId, callback)</td>
    <td style="padding:15px">Get Folder Public Link</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/publiclinks/folder/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12PubliclinksFileFileId(fileId, payload, callback)</td>
    <td style="padding:15px">Create File Public Link</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/publiclinks/file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12PubliclinksFileFileId(fileId, callback)</td>
    <td style="padding:15px">Get File Public Link</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/publiclinks/file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12PubliclinksLinkId(linkId, callback)</td>
    <td style="padding:15px">Get Public Link</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/publiclinks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDocumentsApi12PubliclinksLinkId(linkId, payload, callback)</td>
    <td style="padding:15px">Edit Public Link</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/publiclinks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDocumentsApi12PubliclinksLinkId(linkId, callback)</td>
    <td style="padding:15px">Delete Public Link</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/publiclinks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12Collections(payload, callback)</td>
    <td style="padding:15px">Create a Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/collections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDocumentsApi12CollectionsCollectionId(collectionId, callback)</td>
    <td style="padding:15px">Delete a Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/collections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12CollectionsCollectionIdFilesFileId(collectionId, fileId, callback)</td>
    <td style="padding:15px">Add an Asset to a Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/collections/{pathv1}/files/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDocumentsApi12CollectionsCollectionIdFilesFileId(collectionId, fileId, callback)</td>
    <td style="padding:15px">Remove an Asset from a Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/collections/{pathv1}/files/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12CollectionsCollectionIdFiles(collectionId, payload, callback)</td>
    <td style="padding:15px">Add a List of Assets to a Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/collections/{pathv1}/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDocumentsApi12CollectionsCollectionIdFiles(collectionId, payload, callback)</td>
    <td style="padding:15px">Remove a List of Assets from a Collection</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/collections/{pathv1}/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12CollectionsItems(callback)</td>
    <td style="padding:15px">Retrieve a List of Collections</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/collections/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12ConfigCollaboration(callback)</td>
    <td style="padding:15px">Get Collaboration Configuration</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/config/collaboration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12ConfigSitesDeliverycdn(callback)</td>
    <td style="padding:15px">Get CDN Configuration</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/config/sites/deliverycdn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12ConfigNotificationEmail(callback)</td>
    <td style="padding:15px">Get Email Notification Configuration</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/config/notification/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDocumentsApi12ConfigNotificationEmail(payload, callback)</td>
    <td style="padding:15px">Set Email Notification Configuration</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/config/notification/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12SitesSiteIdSite(siteId, payload, callback)</td>
    <td style="padding:15px">Create Site from Site</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/sites/{pathv1}/site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12TemplatesTemplateIdSite(templateId, payload, callback)</td>
    <td style="padding:15px">Create Site from Template</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/templates/{pathv1}/site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12ApplinksFolderFolderId(folderId, payload, callback)</td>
    <td style="padding:15px">Create Folder Applink</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/applinks/folder/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12ApplinksFileFileId(fileId, payload, callback)</td>
    <td style="padding:15px">Create File Applink</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/applinks/file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDocumentsApi12ApplinksToken(callback)</td>
    <td style="padding:15px">Refresh Applink Token</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/applinks/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi(callback)</td>
    <td style="padding:15px">Get All Versions</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApiVersion(version, callback)</td>
    <td style="padding:15px">Get Specific Version</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApiVersionMetadataCatalog(version, callback)</td>
    <td style="padding:15px">Get API Catalog</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/{pathv1}/metadata-catalog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApiVersionMetadataCatalogFolders(version, callback)</td>
    <td style="padding:15px">Get Folder API Catalog</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/{pathv1}/metadata-catalog/folders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApiVersionMetadataCatalogFiles(version, callback)</td>
    <td style="padding:15px">Get File API Catalog</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/{pathv1}/metadata-catalog/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApiVersionMetadataCatalogUsers(version, callback)</td>
    <td style="padding:15px">Get User API Catalog</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/{pathv1}/metadata-catalog/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApiVersionMetadataCatalogApplinks(version, callback)</td>
    <td style="padding:15px">Get Applink API Catalog</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/{pathv1}/metadata-catalog/applinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApiVersionMetadataCatalogShares(version, callback)</td>
    <td style="padding:15px">Get Share API Catalog</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/{pathv1}/metadata-catalog/shares?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApiVersionMetadataCatalogPubliclinks(version, callback)</td>
    <td style="padding:15px">Get Public Links API Catalog</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/{pathv1}/metadata-catalog/publiclinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApiVersionMetadataCatalogMetadata(version, callback)</td>
    <td style="padding:15px">Get Metadata API Catalog</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/{pathv1}/metadata-catalog/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApiVersionMetadataCatalogConfiguration(version, callback)</td>
    <td style="padding:15px">Get Configuration API Catalog</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/{pathv1}/metadata-catalog/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApiVersionMetadataCatalogCollections(version, callback)</td>
    <td style="padding:15px">Get Collection API Catalog</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/{pathv1}/metadata-catalog/collections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApiVersionMetadataCatalogSites(version, callback)</td>
    <td style="padding:15px">Get Sites API Catalog</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/{pathv1}/metadata-catalog/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApiVersionMetadataCatalogTemplates(version, callback)</td>
    <td style="padding:15px">Get Templates API Catalog</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/{pathv1}/metadata-catalog/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12UsersItems(info, callback)</td>
    <td style="padding:15px">Get Users</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/users/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDocumentsApi12UsersSearchItems(email, callback)</td>
    <td style="padding:15px">Get User with Email Address</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/users/search/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDocumentsApi12UsersUserIDTransferContent(userID, targetUserID, callback)</td>
    <td style="padding:15px">Transfer User Content</td>
    <td style="padding:15px">{base_path}/{version}/documents/api/1.2/users/{pathv1}/transferContent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
