
## 0.2.0 [05-20-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-oracle_cloud!1

---

## 0.1.1 [12-15-2021]

- Initial Commit

See commit 7d8f29b

---
