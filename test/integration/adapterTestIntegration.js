/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-oracle_cloud',
      type: 'OracleCloud',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const OracleCloud = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] OracleCloud Adapter Test', () => {
  describe('OracleCloud Class Tests', () => {
    const a = new OracleCloud(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    let sharesFolderId = 'fakedata';
    const sharesPostDocumentsApi12SharesFolderIdBodyParam = {
      userID: 'string',
      role: 'manager'
    };
    describe('#postDocumentsApi12SharesFolderId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12SharesFolderId(sharesFolderId, sharesPostDocumentsApi12SharesFolderIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('0', data.response.errorCode);
                assert.equal('F1321DC48E3B123D02DBEE88T0000000000100000001', data.response.id);
                assert.equal(true, Array.isArray(data.response.members));
                assert.equal('manager', data.response.role);
                assert.equal('share', data.response.type);
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              sharesFolderId = data.response.id;
              saveMockData('Shares', 'postDocumentsApi12SharesFolderId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sharesGetDocumentsApi12SharesFolderIdItemsBodyParam = {
      currentOnly: 'string'
    };
    describe('#getDocumentsApi12SharesFolderIdItems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12SharesFolderIdItems(sharesFolderId, sharesGetDocumentsApi12SharesFolderIdItemsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1', data.response.count);
                assert.equal('F1321DC48E3B123D02DBEE88T0000000000100000001', data.response.id);
                assert.equal('share', data.response.type);
                assert.equal('0', data.response.errorCode);
                assert.equal('object', typeof data.response.items);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Shares', 'getDocumentsApi12SharesFolderIdItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sharesPutDocumentsApi12SharesFolderIdRoleBodyParam = {
      userID: 'string',
      role: 'contributor'
    };
    describe('#putDocumentsApi12SharesFolderIdRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDocumentsApi12SharesFolderIdRole(sharesFolderId, sharesPutDocumentsApi12SharesFolderIdRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Shares', 'putDocumentsApi12SharesFolderIdRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataCollectionPostDocumentsApi12MetadataSearchFieldsBodyParam = {
      collection: 'string',
      fields: 'string'
    };
    describe('#postDocumentsApi12MetadataSearchFields - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12MetadataSearchFields(metadataCollectionPostDocumentsApi12MetadataSearchFieldsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataCollection', 'postDocumentsApi12MetadataSearchFields', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataCollectionCollectionName = 'fakedata';
    const metadataCollectionPostDocumentsApi12MetadataCollectionNameBodyParam = {
      fields: 'string'
    };
    describe('#postDocumentsApi12MetadataCollectionName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12MetadataCollectionName(metadataCollectionCollectionName, metadataCollectionPostDocumentsApi12MetadataCollectionNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataCollection', 'postDocumentsApi12MetadataCollectionName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataCollectionGetDocumentsApi12MetadataBodyParam = {
      retrieveFields: 5
    };
    describe('#getDocumentsApi12Metadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12Metadata(metadataCollectionGetDocumentsApi12MetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.errorCode);
                assert.equal(true, Array.isArray(data.response.metadataCollections));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataCollection', 'getDocumentsApi12Metadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12MetadataSearchFields - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12MetadataSearchFields((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('metadata', data.response.type);
                assert.equal('0', data.response.errorCode);
                assert.equal('3', data.response.number);
                assert.equal('SearchableCollection.searchField1,SearchableCollection.searchField2,SearchableCollection.searchField3', data.response.fields);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataCollection', 'getDocumentsApi12MetadataSearchFields', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataCollectionPutDocumentsApi12MetadataCollectionNameBodyParam = {
      addFields: 'string',
      addFieldsArray: [
        {
          fieldName: 'string',
          fieldType: 'string',
          fieldDescription: 'string',
          defaultValue: 'string'
        }
      ],
      removeFields: 'string',
      settings: 'string'
    };
    describe('#putDocumentsApi12MetadataCollectionName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDocumentsApi12MetadataCollectionName(metadataCollectionCollectionName, metadataCollectionPutDocumentsApi12MetadataCollectionNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataCollection', 'putDocumentsApi12MetadataCollectionName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12MetadataCollectionName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12MetadataCollectionName(metadataCollectionCollectionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.errorCode);
                assert.equal('string', data.response.isEnabled);
                assert.equal('string', data.response.type);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataCollection', 'getDocumentsApi12MetadataCollectionName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataCollectionPutDocumentsApi12MetadataCollectionNameFieldBodyParam = {
      fields: 'string',
      settings: 'string',
      fieldsArray: [
        {
          fieldName: 'string',
          fieldDescription: 'string',
          defaultValue: 'string',
          isEnabled: true
        }
      ]
    };
    describe('#putDocumentsApi12MetadataCollectionNameField - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDocumentsApi12MetadataCollectionNameField(metadataCollectionCollectionName, metadataCollectionPutDocumentsApi12MetadataCollectionNameFieldBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataCollection', 'putDocumentsApi12MetadataCollectionNameField', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let foldersFolderId = 'fakedata';
    let foldersCollectionName = 'fakedata';
    let foldersJobId = 'fakedata';
    const foldersPostDocumentsApi12FoldersFolderIdBodyParam = {
      name: 'string'
    };
    describe('#postDocumentsApi12FoldersFolderId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderId(foldersFolderId, foldersPostDocumentsApi12FoldersFolderIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('FB4CD874EF94CD2CC1B60B72T0000000000100000001', data.response.id);
                assert.equal('FF4729683CD68C1AFB1AE87DT0000000000100000001', data.response.parentID);
                assert.equal('restFolder', data.response.name);
                assert.equal('rest folder', data.response.description);
                assert.equal('folder', data.response.type);
                assert.equal('0', data.response.size);
                assert.equal('0', data.response.childItemsCount);
                assert.equal('2014-02-21T20:56:07Z', data.response.createdTime);
                assert.equal('2014-02-21T20:59:57Z', data.response.modifiedTime);
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('object', typeof data.response.ownedBy);
                assert.equal('object', typeof data.response.modifiedBy);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              foldersFolderId = data.response.id;
              foldersCollectionName = data.response.name;
              foldersJobId = data.response.id;
              saveMockData('Folders', 'postDocumentsApi12FoldersFolderId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const foldersPostDocumentsApi12FoldersFolderIdBulkCreateBodyParam = {
      folders: [
        {}
      ]
    };
    describe('#postDocumentsApi12FoldersFolderIdBulkCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdBulkCreate(foldersFolderId, foldersPostDocumentsApi12FoldersFolderIdBulkCreateBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(0, data.response.errorCode);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'postDocumentsApi12FoldersFolderIdBulkCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let foldersIdList = 'fakedata';
    const foldersPostDocumentsApi12FoldersFolderIdDownloadBodyParam = {
      idList: 'string'
    };
    describe('#postDocumentsApi12FoldersFolderIdDownload - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdDownload(foldersFolderId, foldersPostDocumentsApi12FoldersFolderIdDownloadBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('OCEHost/documents/api/1.2/folders/FE7DA518D17AA5E46E282D9A1B02AE3398E71823807D/_download/EF45E5E0A9F169D61CA387968719F5AA1614798736483', data.response.Location);
                assert.equal(0, data.response.errorCode);
                assert.equal('fFolderGUID:FE7DA518D17AA5E46E282D9A1B02AE3398E71823807D', data.response.id);
                assert.equal('fFileGUID:D890E0ADF2EB12AAD28D14372E82C5967A21F51BF794,fFolderGUID:FE27E70CA140E819250B4D365F48D86BC9097E77B3EE,fFileGUID:DC9AEA3E7F3AA1C07FCE4E46B7AC2B4F52F010D5A813', data.response.idList);
                assert.equal('FE7DA518D17AA5E46E282D9A1B02AE3398E71823807D,', data.response.parentID);
                assert.equal('folder', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              foldersIdList = data.response.idList;
              saveMockData('Folders', 'postDocumentsApi12FoldersFolderIdDownload', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const foldersPostDocumentsApi12FoldersFolderIdConversationBodyParam = {
      conversationName: 'string'
    };
    describe('#postDocumentsApi12FoldersFolderIdConversation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdConversation(foldersFolderId, foldersPostDocumentsApi12FoldersFolderIdConversationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('F1BDF6BBD9725002B8136F6598503556E71E89DE95CC', data.response.id);
                assert.equal('folder', data.response.type);
                assert.equal('0', data.response.errorCode);
                assert.equal('13841', data.response.conversationID);
                assert.equal('Folder Conversation Test', data.response.conversationName);
                assert.equal('http://service1-tenant1.dly.gemmain.shared.cec.oraclecorp.com:19200/osn/web/cList/conversations/13841', data.response.conversationAppLink);
                assert.equal('e14ad9a457aaa177d33a40115494272bf23bc663072f3fe5cb7412a731df5607', data.response.conversationAppLinkToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'postDocumentsApi12FoldersFolderIdConversation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const foldersPostDocumentsApi12FoldersFolderIdCopyBodyParam = {
      destinationID: 'string'
    };
    describe('#postDocumentsApi12FoldersFolderIdCopy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdCopy(foldersFolderId, foldersPostDocumentsApi12FoldersFolderIdCopyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('FB4CD874EF94CD2CC1B60B72T0000000000100000001', data.response.destinationID);
                assert.equal('FCD4D4A73FA2317583B7C3BFT0000000000100000001', data.response.id);
                assert.equal('F512DA3911603E8AB84A07ACT0000000000100000001', data.response.idList);
                assert.equal('folder', data.response.type);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'postDocumentsApi12FoldersFolderIdCopy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const foldersPostDocumentsApi12FoldersFolderIdMetadataBodyParam = {};
    describe('#postDocumentsApi12FoldersFolderIdMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdMetadata(foldersFolderId, foldersPostDocumentsApi12FoldersFolderIdMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('F94131468853B4845E0508854EE870070A1AA68C3354', data.response.idList);
                assert.equal('folder', data.response.type);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'postDocumentsApi12FoldersFolderIdMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FoldersFolderIdMetadataCollectionName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdMetadataCollectionName(foldersFolderId, foldersCollectionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('F94131468853B4845E0508854EE870070A1AA68C3354', data.response.idList);
                assert.equal('folder', data.response.type);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'postDocumentsApi12FoldersFolderIdMetadataCollectionName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const foldersPostDocumentsApi12FoldersFolderIdMoveBodyParam = {
      destinationID: 'string'
    };
    describe('#postDocumentsApi12FoldersFolderIdMove - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdMove(foldersFolderId, foldersPostDocumentsApi12FoldersFolderIdMoveBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('FB4CD874EF94CD2CC1B60B72T0000000000100000001', data.response.destinationID);
                assert.equal('F512DA3911603E8AB84A07ACT0000000000100000001', data.response.idList);
                assert.equal('folder', data.response.type);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'postDocumentsApi12FoldersFolderIdMove', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const foldersPostDocumentsApi12FoldersFolderIdTagsBodyParam = {
      setTags: 'string'
    };
    describe('#postDocumentsApi12FoldersFolderIdTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdTags(foldersFolderId, foldersPostDocumentsApi12FoldersFolderIdTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('0', data.response.errorCode);
                assert.equal('FD3947BCFB3D3517CC7E6312B5D17B95F47087F4E518', data.response.idList);
                assert.equal('folder', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'postDocumentsApi12FoldersFolderIdTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersItems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FoldersItems(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('3', data.response.count);
                assert.equal('3', data.response.totalResults);
                assert.equal('self', data.response.ownerFolderID);
                assert.equal('0', data.response.hasMore);
                assert.equal('0', data.response.errorCode);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'getDocumentsApi12FoldersItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersLists - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FoldersLists(foldersIdList, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.parentID);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.size);
                assert.equal('string', data.response.childitemsCount);
                assert.equal('string', data.response.createdTime);
                assert.equal('string', data.response.modifiedTime);
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('object', typeof data.response.ownedBy);
                assert.equal('object', typeof data.response.modifiedBy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'getDocumentsApi12FoldersLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const foldersFulltext = 'fakedata';
    const foldersQuerytext = 'fakedata';
    describe('#getDocumentsApi12FoldersSearchItems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FoldersSearchItems(foldersFulltext, foldersQuerytext, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2', data.response.count);
                assert.equal('0', data.response.errorCode);
                assert.equal('1', data.response.offset);
                assert.equal('3', data.response.totalCount);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'getDocumentsApi12FoldersSearchItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const foldersPutDocumentsApi12FoldersFolderIdBodyParam = {
      name: 'string',
      description: 'string'
    };
    describe('#putDocumentsApi12FoldersFolderId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDocumentsApi12FoldersFolderId(foldersFolderId, foldersPutDocumentsApi12FoldersFolderIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'putDocumentsApi12FoldersFolderId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersFolderId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderId(foldersFolderId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2', data.response.childItemsCount);
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('2014-02-21T20:52:37Z', data.response.createdTime);
                assert.equal('0', data.response.errorCode);
                assert.equal('FF4729683CD68C1AFB1AE87DT0000000000100000001', data.response.id);
                assert.equal('object', typeof data.response.modifiedBy);
                assert.equal('2014-02-21T20:52:37Z', data.response.modifiedTime);
                assert.equal('foldera', data.response.name);
                assert.equal('object', typeof data.response.ownedBy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'getDocumentsApi12FoldersFolderId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersFolderIdDownloadJobId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdDownloadJobId(foldersFolderId, foldersJobId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.completed);
                assert.equal(100, data.response.completedPercentage);
                assert.equal('object', typeof data.response.details);
                assert.equal(0, data.response.errorCode);
                assert.equal('object', typeof data.response.links);
                assert.equal('SUCCEEDED', data.response.progress);
                assert.equal('folder', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'getDocumentsApi12FoldersFolderIdDownloadJobId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersFolderIdDownloadJobIdPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdDownloadJobIdPackage(foldersFolderId, foldersJobId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'getDocumentsApi12FoldersFolderIdDownloadJobIdPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersFolderIdItems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdItems(foldersFolderId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2', data.response.count);
                assert.equal('2', data.response.totalResults);
                assert.equal('0', data.response.hasMore);
                assert.equal('0', data.response.errorCode);
                assert.equal('FF4729683CD68C1AFB1AE87DT0000000000100000001', data.response.id);
                assert.equal('self', data.response.parentID);
                assert.equal('foldera', data.response.name);
                assert.equal('rest folder', data.response.description);
                assert.equal('folder', data.response.type);
                assert.equal('39', data.response.size);
                assert.equal('2', data.response.childItemsCount);
                assert.equal('2014-02-21T20:52:37Z', data.response.createdTime);
                assert.equal('2014-02-21T20:52:37Z', data.response.modifiedTime);
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('object', typeof data.response.ownedBy);
                assert.equal('object', typeof data.response.modifiedBy);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'getDocumentsApi12FoldersFolderIdItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersFolderIdMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdMetadata(foldersFolderId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('F94131468853B4845E0508854EE870070A1AA68C3354', data.response.idList);
                assert.equal('folder', data.response.type);
                assert.equal('0', data.response.errorCode);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'getDocumentsApi12FoldersFolderIdMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersFolderIdMetadataFields - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdMetadataFields(foldersFolderId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('F94131468853B4845E0508854EE870070A1AA68C3354', data.response.idList);
                assert.equal('folder', data.response.type);
                assert.equal('0', data.response.errorCode);
                assert.equal('object', typeof data.response.metadataFields);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'getDocumentsApi12FoldersFolderIdMetadataFields', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersFolderIdSearchItems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdSearchItems(foldersFolderId, foldersFulltext, foldersQuerytext, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2', data.response.count);
                assert.equal('0', data.response.errorCode);
                assert.equal('1', data.response.offset);
                assert.equal('2', data.response.totalCount);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'getDocumentsApi12FoldersFolderIdSearchItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const foldersPutDocumentsApi12FoldersFolderIdTagsBodyParam = {
      addTags: 'string',
      removeTags: 'string'
    };
    describe('#putDocumentsApi12FoldersFolderIdTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDocumentsApi12FoldersFolderIdTags(foldersFolderId, foldersPutDocumentsApi12FoldersFolderIdTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'putDocumentsApi12FoldersFolderIdTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersFolderIdTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdTags(foldersFolderId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('0', data.response.errorCode);
                assert.equal('FD3947BCFB3D3517CC7E6312B5D17B95F47087F4E518', data.response.idList);
                assert.equal('todo,pending', data.response.tags);
                assert.equal('folder', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'getDocumentsApi12FoldersFolderIdTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesJsonInputParameters = 'fakedata';
    const filesPrimaryFile = 'fakedata';
    describe('#postDocumentsApi12FilesData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FilesData(filesJsonInputParameters, filesPrimaryFile, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('DB1C5AF480FFE61C588027A8T0000000000100000001', data.response.id);
                assert.equal('FF4729683CD68C1AFB1AE87DT0000000000100000001', data.response.parentID);
                assert.equal('example.txt', data.response.name);
                assert.equal('file', data.response.type);
                assert.equal('13', data.response.size);
                assert.equal('1', data.response.version);
                assert.equal('2014-02-21T21:32:37Z', data.response.createdTime);
                assert.equal('2014-02-21T21:32:37Z', data.response.modifiedTime);
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('object', typeof data.response.ownedBy);
                assert.equal('object', typeof data.response.modifiedBy);
                assert.equal('0', data.response.errorCode);
                assert.equal('Successfully checked in content item &#39;ADC412030000000000000000000002&#39;.', data.response.errorMessage);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'postDocumentsApi12FilesData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let filesFileId = 'fakedata';
    const filesPostDocumentsApi12FilesFileIdConversationBodyParam = {
      conversationName: 'string'
    };
    describe('#postDocumentsApi12FilesFileIdConversation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdConversation(filesFileId, filesPostDocumentsApi12FilesFileIdConversationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('DFCCA263ED167730B7824BFF98503556E71E89DE95CC', data.response.id);
                assert.equal('file', data.response.type);
                assert.equal('0', data.response.errorCode);
                assert.equal('13865', data.response.conversationID);
                assert.equal('File Conversation Test', data.response.conversationName);
                assert.equal('http://service1-tenant1.dly.gemmain.shared.cec.oraclecorp.com:19200/osn/web/cList/conversations/13865', data.response.conversationAppLink);
                assert.equal('2842038cbf8b9c5db8ec37db134b5af44e9240d20f79891a0eb18e3df90b6684s', data.response.conversationAppLinkToken);
              } else {
                runCommonAsserts(data, error);
              }
              filesFileId = data.response.id;
              saveMockData('Files', 'postDocumentsApi12FilesFileIdConversation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let filesIdList = 'fakedata';
    const filesPostDocumentsApi12FilesFileIdCopyBodyParam = {
      destinationID: 'string'
    };
    describe('#postDocumentsApi12FilesFileIdCopy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdCopy(filesFileId, filesPostDocumentsApi12FilesFileIdCopyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('FF4729683CD68C1AFB1AE87DT0000000000100000001', data.response.destinationID);
                assert.equal('D4018B1EF3AC07D0A349DB8DT0000000000100000001', data.response.id);
                assert.equal('D574378400573ED9D62B3195T0000000000100000001', data.response.idList);
                assert.equal('file', data.response.type);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              filesIdList = data.response.idList;
              saveMockData('Files', 'postDocumentsApi12FilesFileIdCopy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FilesFileIdData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdData(filesFileId, filesPrimaryFile, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D574378400573ED9D62B3195T0000000000100000001', data.response.id);
                assert.equal('FB4CD874EF94CD2CC1B60B72T0000000000100000001', data.response.parentID);
                assert.equal('example2.txt', data.response.name);
                assert.equal('file', data.response.type);
                assert.equal('13', data.response.size);
                assert.equal('1', data.response.version);
                assert.equal('2014-02-21T21:15:57Z', data.response.createdTime);
                assert.equal('2014-02-21T21:18:20Z', data.response.modifiedTime);
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('object', typeof data.response.ownedBy);
                assert.equal('object', typeof data.response.modifiedBy);
                assert.equal('0', data.response.errorCode);
                assert.equal('Successfully checked in content item &#39;ADC412030000000000000000000003&#39;.', data.response.errorMessage);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'postDocumentsApi12FilesFileIdData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesFile = 'fakedata';
    describe('#postDocumentsApi12FilesFileIdDataRendition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdDataRendition(filesFileId, filesJsonInputParameters, filesFile, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D3C1C1F319CFE6B102095C5DT0000000000100000001', data.response.id);
                assert.equal('file', data.response.type);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'postDocumentsApi12FilesFileIdDataRendition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesPostDocumentsApi12FilesFileIdMetadataBodyParam = {};
    describe('#postDocumentsApi12FilesFileIdMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdMetadata(filesFileId, filesPostDocumentsApi12FilesFileIdMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D3C1C1F319CFE6B102095C5DT0000000000100000001', data.response.idList);
                assert.equal('file', data.response.type);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'postDocumentsApi12FilesFileIdMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesCollectionName = 'fakedata';
    describe('#postDocumentsApi12FilesFileIdMetadataCollectionName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdMetadataCollectionName(filesFileId, filesCollectionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D3C1C1F319CFE6B102095C5DT0000000000100000001', data.response.idList);
                assert.equal('file', data.response.type);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'postDocumentsApi12FilesFileIdMetadataCollectionName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesPostDocumentsApi12FilesFileIdMoveBodyParam = {
      destinationID: 'string'
    };
    describe('#postDocumentsApi12FilesFileIdMove - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdMove(filesFileId, filesPostDocumentsApi12FilesFileIdMoveBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('FF4729683CD68C1AFB1AE87DT0000000000100000001', data.response.destinationID);
                assert.equal('D574378400573ED9D62B3195T0000000000100000001', data.response.idList);
                assert.equal('file', data.response.type);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'postDocumentsApi12FilesFileIdMove', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FilesFileIdPages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdPages(filesFileId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D3C1C1F319CFE6B102095C5DT0000000000100000001', data.response.id);
                assert.equal('file', data.response.type);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'postDocumentsApi12FilesFileIdPages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FilesFileIdReserve - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdReserve(filesFileId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D5B8B54BA8C121BEDB91A1341212FF6185DEA5EFC0BD', data.response.id);
                assert.equal('2015-06-15T19:34:17.000Z', data.response.reservationDate);
                assert.equal('object', typeof data.response.reservedBy);
                assert.equal('file', data.response.type);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'postDocumentsApi12FilesFileIdReserve', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesPostDocumentsApi12FilesFileIdTagsBodyParam = {
      setTags: 'string'
    };
    describe('#postDocumentsApi12FilesFileIdTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdTags(filesFileId, filesPostDocumentsApi12FilesFileIdTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('0', data.response.errorCode);
                assert.equal('D3C1C1F319CFE6B102095C5DT0000000000100000001', data.response.idList);
                assert.equal('file', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'postDocumentsApi12FilesFileIdTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesPostDocumentsApi12FilesFileIdUnreserveBodyParam = {
      reservedByUserID: 'string'
    };
    describe('#postDocumentsApi12FilesFileIdUnreserve - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdUnreserve(filesFileId, filesPostDocumentsApi12FilesFileIdUnreserveBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D5B8B54BA8C121BEDB91A1341212FF6185DEA5EFC0BD', data.response.id);
                assert.equal('file', data.response.type);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'postDocumentsApi12FilesFileIdUnreserve', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesLists - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FilesLists(filesIdList, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('0', data.response.errorCode);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesPutDocumentsApi12FilesFileIdBodyParam = {
      name: 'string'
    };
    describe('#putDocumentsApi12FilesFileId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDocumentsApi12FilesFileId(filesFileId, filesPutDocumentsApi12FilesFileIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'putDocumentsApi12FilesFileId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FilesFileId(filesFileId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('13', data.response.count);
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('2018-02-28T20:00:22Z', data.response.createdTime);
                assert.equal('0', data.response.errorCode);
                assert.equal('DFB10CF19A743BF1338CFF01AB7008277F375F0158F1', data.response.id);
                assert.equal('image/jpeg', data.response.mimeType);
                assert.equal('object', typeof data.response.modifiedBy);
                assert.equal('2018-02-28T20:00:22Z', data.response.modifiedTime);
                assert.equal('BlueSquare.jpg', data.response.name);
                assert.equal('object', typeof data.response.ownedBy);
                assert.equal('F26D598E507E96F9B41C98BA36219C7DA83E60B83105', data.response.parentID);
                assert.equal('24205', data.response.size);
                assert.equal('file', data.response.type);
                assert.equal('1', data.response.version);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesFileId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdAccesses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdAccesses(filesFileId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1', data.response.count);
                assert.equal('0', data.response.errorCode);
                assert.equal('0', data.response.hasMore);
                assert.equal('1', data.response.totalCount);
                assert.equal('access', data.response.type);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesFileIdAccesses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdData - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdData(filesFileId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesFileIdData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdDataLargeImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdDataLargeImage(filesFileId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesFileIdDataLargeImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdDataMediumImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdDataMediumImage(filesFileId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesFileIdDataMediumImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesRendition = 'fakedata';
    describe('#getDocumentsApi12FilesFileIdDataRendition - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdDataRendition(filesFileId, filesRendition, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesFileIdDataRendition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdDataRenditions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdDataRenditions(filesFileId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('0', data.response.errorCode);
                assert.equal('D3C1C1F319CFE6B102095C5DT0000000000100000001', data.response.id);
                assert.equal('file', data.response.type);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesFileIdDataRenditions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdDataSmallImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdDataSmallImage(filesFileId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesFileIdDataSmallImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdDataThumbnail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdDataThumbnail(filesFileId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesFileIdDataThumbnail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdDataThumbnailImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdDataThumbnailImage(filesFileId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesFileIdDataThumbnailImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdMetadata(filesFileId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D3C1C1F319CFE6B102095C5DT0000000000100000001', data.response.idList);
                assert.equal('file', data.response.type);
                assert.equal('0', data.response.errorCode);
                assert.equal('object', typeof data.response.metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesFileIdMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdMetadataFields - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdMetadataFields(filesFileId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D3C1C1F319CFE6B102095C5DT0000000000100000001', data.response.idList);
                assert.equal('file', data.response.type);
                assert.equal('0', data.response.errorCode);
                assert.equal('object', typeof data.response.metadataFields);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesFileIdMetadataFields', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdPages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdPages(filesFileId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D3C1C1F319CFE6B102095C5DT0000000000100000001', data.response.id);
                assert.equal('22', data.response.pages);
                assert.equal('file', data.response.type);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesFileIdPages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdPreviewPath - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdPreviewPath(filesFileId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('0', data.response.errorCode);
                assert.equal('D3C1C1F319CFE6B102095C5DT0000000000100000001', data.response.id);
                assert.equal('http://Service1-AttTenant1.slc04ymr.us.oracle.com:12020/documents/fileview/D03175B85C8EAA3B6F2BC6A84EE870070A1A2B7FEACF/1/preview/html5/pvw.html', data.response.previewUrl);
                assert.equal('1', data.response.version);
                assert.equal('file', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesFileIdPreviewPath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesPutDocumentsApi12FilesFileIdTagsBodyParam = {
      addTags: 'string',
      removeTags: 'string'
    };
    describe('#putDocumentsApi12FilesFileIdTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDocumentsApi12FilesFileIdTags(filesFileId, filesPutDocumentsApi12FilesFileIdTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'putDocumentsApi12FilesFileIdTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdTags(filesFileId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('0', data.response.errorCode);
                assert.equal('D3C1C1F319CFE6B102095C5DT0000000000100000001', data.response.idList);
                assert.equal('personal,work', data.response.tags);
                assert.equal('file', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesFileIdTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdVersions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdVersions(filesFileId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D574378400573ED9D62B3195T0000000000100000001', data.response.id);
                assert.equal('FB4CD874EF94CD2CC1B60B72T0000000000100000001', data.response.parentID);
                assert.equal('example2.txt', data.response.name);
                assert.equal('file', data.response.type);
                assert.equal('0', data.response.errorCode);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getDocumentsApi12FilesFileIdVersions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let publiclinksFileId = 'fakedata';
    const publiclinksPostDocumentsApi12PubliclinksFileFileIdBodyParam = {
      assignedUsers: 'string'
    };
    describe('#postDocumentsApi12PubliclinksFileFileId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12PubliclinksFileFileId(publiclinksFileId, publiclinksPostDocumentsApi12PubliclinksFileFileIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('LDFD004B846DB106DB8B2906T0000000000100000001', data.response.linkID);
                assert.equal('MyFileLinkOne', data.response.linkName);
                assert.equal('@everybody', data.response.assignedUsers);
                assert.equal('contributor', data.response.role);
                assert.equal('publiclink', data.response.type);
                assert.equal('2015-06-10T16:13:19Z', data.response.createdTime);
                assert.equal('2017-01-01T00:00:01Z', data.response.expirationTime);
                assert.equal('2015-06-10T16:13:19Z', data.response.lastModifiedTime);
                assert.equal('object', typeof data.response.ownedBy);
                assert.equal('0', data.response.errorCode);
                assert.equal('D1E1E9F089AC1EF8481E5B94T0000000000100000001', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              publiclinksFileId = data.response.id;
              saveMockData('Publiclinks', 'postDocumentsApi12PubliclinksFileFileId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let publiclinksFolderId = 'fakedata';
    const publiclinksPostDocumentsApi12PubliclinksFolderFolderIdBodyParam = {
      assignedUsers: 'string'
    };
    describe('#postDocumentsApi12PubliclinksFolderFolderId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12PubliclinksFolderFolderId(publiclinksFolderId, publiclinksPostDocumentsApi12PubliclinksFolderFolderIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('LF906748A021ACD714CABC82T0000000000100000001', data.response.linkID);
                assert.equal('MyLinkOne', data.response.linkName);
                assert.equal('@serviceinstance', data.response.assignedUsers);
                assert.equal('contributor', data.response.role);
                assert.equal('publiclink', data.response.type);
                assert.equal('2015-06-10T16:01:44Z', data.response.createdTime);
                assert.equal('2016-01-01T00:00:01Z', data.response.expirationTime);
                assert.equal('2015-06-10T16:01:44Z', data.response.lastModifiedTime);
                assert.equal('object', typeof data.response.ownedBy);
                assert.equal('0', data.response.errorCode);
                assert.equal('FBE1117A270E747BB1D95024T0000000000100000001', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              publiclinksFolderId = data.response.id;
              saveMockData('Publiclinks', 'postDocumentsApi12PubliclinksFolderFolderId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12PubliclinksFileFileId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12PubliclinksFileFileId(publiclinksFileId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2', data.response.count);
                assert.equal('D1E1E9F089AC1EF8481E5B94T0000000000100000001', data.response.id);
                assert.equal('file', data.response.type);
                assert.equal('0', data.response.errorCode);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Publiclinks', 'getDocumentsApi12PubliclinksFileFileId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12PubliclinksFolderFolderId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12PubliclinksFolderFolderId(publiclinksFolderId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2', data.response.count);
                assert.equal('F4E111D0D0645CD368453C2BT0000000000100000001', data.response.id);
                assert.equal('folder', data.response.type);
                assert.equal('0', data.response.errorCode);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Publiclinks', 'getDocumentsApi12PubliclinksFolderFolderId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const publiclinksLinkId = 'fakedata';
    const publiclinksPutDocumentsApi12PubliclinksLinkIdBodyParam = {
      assignedUsers: 'string',
      expirationTime: 'string',
      password: 'string',
      role: 'contributor'
    };
    describe('#putDocumentsApi12PubliclinksLinkId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDocumentsApi12PubliclinksLinkId(publiclinksLinkId, publiclinksPutDocumentsApi12PubliclinksLinkIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Publiclinks', 'putDocumentsApi12PubliclinksLinkId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12PubliclinksLinkId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12PubliclinksLinkId(publiclinksLinkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('LF906748A021ACD714CABC82T0000000000100000001', data.response.linkID);
                assert.equal('MyLinkOne', data.response.linkName);
                assert.equal('@everybody', data.response.assignedUsers);
                assert.equal('contributor', data.response.role);
                assert.equal('publiclink', data.response.type);
                assert.equal('2015-06-10T16:01:44Z', data.response.createdTime);
                assert.equal('2016-01-01T00:00:01Z', data.response.expirationTime);
                assert.equal('2015-06-10T16:01:44Z', data.response.lastModifiedTime);
                assert.equal('object', typeof data.response.ownedBy);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Publiclinks', 'getDocumentsApi12PubliclinksLinkId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let collectionsCollectionId = 'fakedata';
    let collectionsFileId = 'fakedata';
    const collectionsPostDocumentsApi12CollectionsBodyParam = {
      name: 'string'
    };
    describe('#postDocumentsApi12Collections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12Collections(collectionsPostDocumentsApi12CollectionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('0', data.response.childItemsCount);
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('2017-03-28T20:09:21Z', data.response.createdTime);
                assert.equal('Optional description', data.response.description);
                assert.equal('0', data.response.errorCode);
                assert.equal('F79BD107FC398ED6FB482274T0000DEFAULT00000000', data.response.id);
                assert.equal('object', typeof data.response.modifiedBy);
                assert.equal('2017-03-28T20:09:21Z', data.response.modifiedTime);
                assert.equal('myCollection', data.response.name);
                assert.equal('object', typeof data.response.ownedBy);
                assert.equal('collection', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              collectionsCollectionId = data.response.id;
              collectionsFileId = data.response.id;
              saveMockData('Collections', 'postDocumentsApi12Collections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionsPostDocumentsApi12CollectionsCollectionIdFilesBodyParam = {
      idList: 'string',
      skipDuplicate: false,
      skipInaccessible: false,
      skipNotAllowed: false
    };
    describe('#postDocumentsApi12CollectionsCollectionIdFiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12CollectionsCollectionIdFiles(collectionsCollectionId, collectionsPostDocumentsApi12CollectionsCollectionIdFilesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('4', data.response.count);
                assert.equal('0', data.response.errorCode);
                assert.equal('F7A27C75D2960E169E902F9D45B3E9DD6490CF81DFC2', data.response.id);
                assert.equal('DC9B560D7F2056632D6C651745B3E9DD6490CF81DFC2,DB538D72C3A1B532A163AD0A45B3E9DD6490CF81DFC2,D908B9357C4E7C64D3370A0D45B3E9DD6490CF81DFC2,D6687F6592E2186E4FFC298945B3E9DD6490CF81DFC2', data.response.idList);
                assert.equal('collection', data.response.type);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collections', 'postDocumentsApi12CollectionsCollectionIdFiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12CollectionsCollectionIdFilesFileId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12CollectionsCollectionIdFilesFileId(collectionsCollectionId, collectionsFileId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('0', data.response.errorCode);
                assert.equal('F072ECEAE8DFE10499D8C160T0000DEFAULT00000000', data.response.id);
                assert.equal('DDEA5A0845C4628E6DAAE369T0000DEFAULT00000000', data.response.itemID);
                assert.equal('collection', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collections', 'postDocumentsApi12CollectionsCollectionIdFilesFileId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12CollectionsItems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12CollectionsItems((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1', data.response.count);
                assert.equal('0', data.response.errorCode);
                assert.equal('0', data.response.hasMore);
                assert.equal('0', data.response.offset);
                assert.equal('1', data.response.totalOwned);
                assert.equal('1', data.response.totalResults);
                assert.equal('0', data.response.totalShared);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collections', 'getDocumentsApi12CollectionsItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12ConfigCollaboration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12ConfigCollaboration((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('0', data.response.errorCode);
                assert.equal('http://service1-tenant1.dly.gemmain.shared.osn.oraclecorp.com:19200/osn/web/cList', data.response.cListURL);
                assert.equal('http://service1-tenant1.dly.gemmain.shared.osn.oraclecorp.com:19200/osn', data.response.clientURL);
                assert.equal('http://service1-tenant1.dly.gemmain.shared.osn.oraclecorp.com:19200/osnoauth/web/cList', data.response.oAuthCListURL);
                assert.equal('http://service1-tenant1.dly.gemmain.shared.osn.oraclecorp.com:19200/osnoauth', data.response.oAuthClientURL);
                assert.equal('http://service1-tenant1.dly.gemmain.shared.osn.oraclecorp.com:19200/osn/social/api/v1', data.response.restURL);
                assert.equal('http://service1-tenant1.dly.gemmain.shared.osn.oraclecorp.com:19200/osn', data.response.serviceURL);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configuration', 'getDocumentsApi12ConfigCollaboration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationPutDocumentsApi12ConfigNotificationEmailBodyParam = {
      disableEmailNotifications: 'string'
    };
    describe('#putDocumentsApi12ConfigNotificationEmail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDocumentsApi12ConfigNotificationEmail(configurationPutDocumentsApi12ConfigNotificationEmailBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configuration', 'putDocumentsApi12ConfigNotificationEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12ConfigNotificationEmail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12ConfigNotificationEmail((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('0', data.response.errorCode);
                assert.equal('false', data.response.disableEmailNotifications);
                assert.equal('config', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configuration', 'getDocumentsApi12ConfigNotificationEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12ConfigSitesDeliverycdn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12ConfigSitesDeliverycdn((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('0', data.response.errorCode);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Configuration', 'getDocumentsApi12ConfigSitesDeliverycdn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let sitesSiteId = 'fakedata';
    const sitesPostDocumentsApi12SitesSiteIdSiteBodyParam = {
      name: 'string'
    };
    describe('#postDocumentsApi12SitesSiteIdSite - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12SitesSiteIdSite(sitesSiteId, sitesPostDocumentsApi12SitesSiteIdSiteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('2017-06-12T21:19:30Z', data.response.createdTime);
                assert.equal('Site created from an existing site', data.response.description);
                assert.equal('0', data.response.errorCode);
                assert.equal('F4FAEFCE084DD434A61B7CA9B5D17B95F47087F4E518', data.response.id);
                assert.equal('object', typeof data.response.modifiedBy);
                assert.equal('2017-06-12T21:19:30Z', data.response.modifiedTime);
                assert.equal('Site Sample from Another Site', data.response.name);
                assert.equal('object', typeof data.response.ownedBy);
                assert.equal('object', typeof data.response.pseudoOwnedBy);
                assert.equal('site', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              sitesSiteId = data.response.id;
              saveMockData('Sites', 'postDocumentsApi12SitesSiteIdSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let templatesTemplateId = 'fakedata';
    const templatesPostDocumentsApi12TemplatesTemplateIdSiteBodyParam = {
      name: 'string'
    };
    describe('#postDocumentsApi12TemplatesTemplateIdSite - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12TemplatesTemplateIdSite(templatesTemplateId, templatesPostDocumentsApi12TemplatesTemplateIdSiteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('2017-06-12T21:05:10Z', data.response.createdTime);
                assert.equal('Site created from an existing template', data.response.description);
                assert.equal('0', data.response.errorCode);
                assert.equal('F4FAEFCE084DD434A61B7CA9B5D17B95F47087F4E518', data.response.id);
                assert.equal('object', typeof data.response.modifiedBy);
                assert.equal('2017-06-12T21:05:10Z', data.response.modifiedTime);
                assert.equal('Site Sample from Template', data.response.name);
                assert.equal('object', typeof data.response.ownedBy);
                assert.equal('object', typeof data.response.pseudoOwnedBy);
                assert.equal('site', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              templatesTemplateId = data.response.id;
              saveMockData('Templates', 'postDocumentsApi12TemplatesTemplateIdSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let applinksFileId = 'fakedata';
    const applinksPostDocumentsApi12ApplinksFileFileIdBodyParam = {
      assignedUser: 'string'
    };
    describe('#postDocumentsApi12ApplinksFileFileId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12ApplinksFileFileId(applinksFileId, applinksPostDocumentsApi12ApplinksFileFileIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('LDhsn4VPTsnDnKpKLFZTCkjaPkYbMC6-3taYSdJAazckhezJ2HlSjs2THOou6cCAvxcRnw5gpXcU7pIRkCmWN8kEToJHFwwZ-ptWvPGhJaiirl9baL9mka14WnwpL6auOO40-gFMPvkPv23OtMnj2W3A==', data.response.appLinkID);
                assert.equal('GYrSN5zuj0kOTE4k_60bKvdkxx2-ARA546A2T77GtEOgoPZPGgKk126OeCn1w-Ij', data.response.accessToken);
                assert.equal('http://www.example.com/documents/embed/link/app/LDhsn4VPTsnDnKpKLFZTCkjaPkYbMC6-3taYSdJAazckhezJ2HlSjs2THOou6cCAvxcRnw5gpXcU7pIRkCmWN8kEToJHFwwZ-ptWvPGhJaiirl9baL9mka14WnwpL6auOO40-gFMPvkPv23OtMnj2W3A==/fileview/DFD11F62E911327CB1F160F6T0000000000100000001', data.response.appLinkUrl);
                assert.equal('Yc_b_dE8V03eDTCmcmC1gi_y3LVJTPiZOSQDhuS_VWim9E_FRpLQGtEhgxCNbKTG', data.response.refreshToken);
                assert.equal('manager', data.response.role);
                assert.equal('DFD11F62E911327CB1F160F6T0000000000100000001', data.response.id);
                assert.equal('applink', data.response.type);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              applinksFileId = data.response.id;
              saveMockData('Applinks', 'postDocumentsApi12ApplinksFileFileId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let applinksFolderId = 'fakedata';
    const applinksPostDocumentsApi12ApplinksFolderFolderIdBodyParam = {
      assignedUser: 'string'
    };
    describe('#postDocumentsApi12ApplinksFolderFolderId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12ApplinksFolderFolderId(applinksFolderId, applinksPostDocumentsApi12ApplinksFolderFolderIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('LFwi1u1E9vZC_3pQhK5C4qxigAC4o1eUuMnv3dZbo6_OiaZx-yte-xIrxWdDJCA_jEgfVHe1At-26KX-M9mi9BaI7B0UX3Gx-QufREHWcHakby0_V9n8_C1pT7P_CKYlSqFU0rudQ8Q81M67-3fLffMA==', data.response.appLinkID);
                assert.equal('_qVnD42Et8KwZrBUqveiqn1sw-RLpfZQdIPPi_fd17-gt60FP6-IuCApyLFw32EL', data.response.accessToken);
                assert.equal('ctOCKm4mtpayp__jUlQsfdFEd52vSn2Pd4QxOTcSZ1Wch0f9JP7EFSaXmDLKZdql', data.response.refreshToken);
                assert.equal('http://www.example.com/documents/embed/link/app/LFwi1u1E9vZC_3pQhK5C4qxigAC4o1eUuMnv3dZbo6_OiaZx-yte-xIrxWdDJCA_jEgfVHe1At-26KX-M9mi9BaI7B0UX3Gx-QufREHWcHakby0_V9n8_C1pT7P_CKYlSqFU0rudQ8Q81M67-3fLffMA==/folder/F3399E673E5B93B3774531E7T0000000000100000001', data.response.appLinkUrl);
                assert.equal('viewer', data.response.role);
                assert.equal('F3399E673E5B93B3774531E7T0000000000100000001', data.response.id);
                assert.equal('applink', data.response.type);
                assert.equal('0', data.response.errorCode);
              } else {
                runCommonAsserts(data, error);
              }
              applinksFolderId = data.response.id;
              saveMockData('Applinks', 'postDocumentsApi12ApplinksFolderFolderId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDocumentsApi12ApplinksToken - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDocumentsApi12ApplinksToken((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Applinks', 'putDocumentsApi12ApplinksToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Catalog', 'getDocumentsApi', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const catalogVersion = 'fakedata';
    describe('#getDocumentsApiVersion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApiVersion(catalogVersion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Catalog', 'getDocumentsApiVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalog - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalog(catalogVersion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Catalog', 'getDocumentsApiVersionMetadataCatalog', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogApplinks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogApplinks(catalogVersion, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Catalog', 'getDocumentsApiVersionMetadataCatalogApplinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogCollections - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogCollections(catalogVersion, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Catalog', 'getDocumentsApiVersionMetadataCatalogCollections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogConfiguration(catalogVersion, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Catalog', 'getDocumentsApiVersionMetadataCatalogConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogFiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogFiles(catalogVersion, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Catalog', 'getDocumentsApiVersionMetadataCatalogFiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogFolders - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogFolders(catalogVersion, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Catalog', 'getDocumentsApiVersionMetadataCatalogFolders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogMetadata - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogMetadata(catalogVersion, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Catalog', 'getDocumentsApiVersionMetadataCatalogMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogPubliclinks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogPubliclinks(catalogVersion, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Catalog', 'getDocumentsApiVersionMetadataCatalogPubliclinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogShares - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogShares(catalogVersion, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Catalog', 'getDocumentsApiVersionMetadataCatalogShares', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogSites - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogSites(catalogVersion, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Catalog', 'getDocumentsApiVersionMetadataCatalogSites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogTemplates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogTemplates(catalogVersion, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Catalog', 'getDocumentsApiVersionMetadataCatalogTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogUsers(catalogVersion, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Catalog', 'getDocumentsApiVersionMetadataCatalogUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUserID = 'fakedata';
    const usersTargetUserID = 'fakedata';
    describe('#postDocumentsApi12UsersUserIDTransferContent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDocumentsApi12UsersUserIDTransferContent(usersUserID, usersTargetUserID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('0', data.response.errorCode);
                assert.equal('object', typeof data.response.sourceUser);
                assert.equal('object', typeof data.response.targetUser);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'postDocumentsApi12UsersUserIDTransferContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersInfo = 'fakedata';
    describe('#getDocumentsApi12UsersItems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12UsersItems(usersInfo, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('3', data.response.count);
                assert.equal('0', data.response.errorCode);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getDocumentsApi12UsersItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersEmail = 'fakedata';
    describe('#getDocumentsApi12UsersSearchItems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDocumentsApi12UsersSearchItems(usersEmail, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1', data.response.count);
                assert.equal('0', data.response.errorCode);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getDocumentsApi12UsersSearchItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sharesDeleteDocumentsApi12SharesFolderIdBodyParam = {
      message: 'string'
    };
    describe('#deleteDocumentsApi12SharesFolderId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDocumentsApi12SharesFolderId(sharesFolderId, sharesDeleteDocumentsApi12SharesFolderIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Shares', 'deleteDocumentsApi12SharesFolderId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12SharesFolderIdMyuser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDocumentsApi12SharesFolderIdMyuser(sharesFolderId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Shares', 'deleteDocumentsApi12SharesFolderIdMyuser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sharesDeleteDocumentsApi12SharesFolderIdUserBodyParam = {
      userID: 'string'
    };
    describe('#deleteDocumentsApi12SharesFolderIdUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDocumentsApi12SharesFolderIdUser(sharesFolderId, sharesDeleteDocumentsApi12SharesFolderIdUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Shares', 'deleteDocumentsApi12SharesFolderIdUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12MetadataCollectionName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDocumentsApi12MetadataCollectionName(metadataCollectionCollectionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataCollection', 'deleteDocumentsApi12MetadataCollectionName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12FoldersFolderId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDocumentsApi12FoldersFolderId(foldersFolderId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'deleteDocumentsApi12FoldersFolderId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12FoldersFolderIdDownloadJobId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDocumentsApi12FoldersFolderIdDownloadJobId(foldersFolderId, foldersJobId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'deleteDocumentsApi12FoldersFolderIdDownloadJobId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const foldersDeleteDocumentsApi12FoldersFolderIdMetadataBodyParam = {
      collection: 'string',
      fields: 'string'
    };
    describe('#deleteDocumentsApi12FoldersFolderIdMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDocumentsApi12FoldersFolderIdMetadata(foldersFolderId, foldersDeleteDocumentsApi12FoldersFolderIdMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'deleteDocumentsApi12FoldersFolderIdMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12FoldersFolderIdTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDocumentsApi12FoldersFolderIdTags(foldersFolderId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Folders', 'deleteDocumentsApi12FoldersFolderIdTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12FilesFileId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDocumentsApi12FilesFileId(filesFileId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'deleteDocumentsApi12FilesFileId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12FilesFileIdDataRendition - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDocumentsApi12FilesFileIdDataRendition(filesFileId, filesRendition, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-oracle_cloud-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'deleteDocumentsApi12FilesFileIdDataRendition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesDeleteDocumentsApi12FilesFileIdMetadataBodyParam = {
      collection: 'string',
      fields: 'string'
    };
    describe('#deleteDocumentsApi12FilesFileIdMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDocumentsApi12FilesFileIdMetadata(filesFileId, filesDeleteDocumentsApi12FilesFileIdMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'deleteDocumentsApi12FilesFileIdMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12FilesFileIdTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDocumentsApi12FilesFileIdTags(filesFileId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'deleteDocumentsApi12FilesFileIdTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12PubliclinksLinkId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDocumentsApi12PubliclinksLinkId(publiclinksLinkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Publiclinks', 'deleteDocumentsApi12PubliclinksLinkId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12CollectionsCollectionId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDocumentsApi12CollectionsCollectionId(collectionsCollectionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collections', 'deleteDocumentsApi12CollectionsCollectionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const collectionsDeleteDocumentsApi12CollectionsCollectionIdFilesBodyParam = {
      idList: 'string',
      skipIfNotInCollection: true
    };
    describe('#deleteDocumentsApi12CollectionsCollectionIdFiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDocumentsApi12CollectionsCollectionIdFiles(collectionsCollectionId, collectionsDeleteDocumentsApi12CollectionsCollectionIdFilesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collections', 'deleteDocumentsApi12CollectionsCollectionIdFiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12CollectionsCollectionIdFilesFileId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDocumentsApi12CollectionsCollectionIdFilesFileId(collectionsCollectionId, collectionsFileId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Collections', 'deleteDocumentsApi12CollectionsCollectionIdFilesFileId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
