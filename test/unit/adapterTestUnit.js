/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-oracle_cloud',
      type: 'OracleCloud',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const OracleCloud = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] OracleCloud Adapter Test', () => {
  describe('OracleCloud Class Tests', () => {
    const a = new OracleCloud(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('oracle_cloud'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('oracle_cloud'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('OracleCloud', pronghornDotJson.export);
          assert.equal('OracleCloud', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-oracle_cloud', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('oracle_cloud'));
          assert.equal('OracleCloud', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-oracle_cloud', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-oracle_cloud', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#postDocumentsApi12SharesFolderId - errors', () => {
      it('should have a postDocumentsApi12SharesFolderId function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12SharesFolderId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.postDocumentsApi12SharesFolderId(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12SharesFolderId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12SharesFolderId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12SharesFolderId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12SharesFolderId - errors', () => {
      it('should have a deleteDocumentsApi12SharesFolderId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDocumentsApi12SharesFolderId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.deleteDocumentsApi12SharesFolderId(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12SharesFolderId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.deleteDocumentsApi12SharesFolderId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12SharesFolderId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12SharesFolderIdItems - errors', () => {
      it('should have a getDocumentsApi12SharesFolderIdItems function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12SharesFolderIdItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.getDocumentsApi12SharesFolderIdItems(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12SharesFolderIdItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.getDocumentsApi12SharesFolderIdItems('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12SharesFolderIdItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDocumentsApi12SharesFolderIdRole - errors', () => {
      it('should have a putDocumentsApi12SharesFolderIdRole function', (done) => {
        try {
          assert.equal(true, typeof a.putDocumentsApi12SharesFolderIdRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.putDocumentsApi12SharesFolderIdRole(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-putDocumentsApi12SharesFolderIdRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.putDocumentsApi12SharesFolderIdRole('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-putDocumentsApi12SharesFolderIdRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12SharesFolderIdUser - errors', () => {
      it('should have a deleteDocumentsApi12SharesFolderIdUser function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDocumentsApi12SharesFolderIdUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.deleteDocumentsApi12SharesFolderIdUser(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12SharesFolderIdUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.deleteDocumentsApi12SharesFolderIdUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12SharesFolderIdUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12SharesFolderIdMyuser - errors', () => {
      it('should have a deleteDocumentsApi12SharesFolderIdMyuser function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDocumentsApi12SharesFolderIdMyuser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.deleteDocumentsApi12SharesFolderIdMyuser(null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12SharesFolderIdMyuser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12MetadataCollectionName - errors', () => {
      it('should have a postDocumentsApi12MetadataCollectionName function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12MetadataCollectionName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectionName', (done) => {
        try {
          a.postDocumentsApi12MetadataCollectionName(null, null, (data, error) => {
            try {
              const displayE = 'collectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12MetadataCollectionName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12MetadataCollectionName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12MetadataCollectionName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDocumentsApi12MetadataCollectionName - errors', () => {
      it('should have a putDocumentsApi12MetadataCollectionName function', (done) => {
        try {
          assert.equal(true, typeof a.putDocumentsApi12MetadataCollectionName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectionName', (done) => {
        try {
          a.putDocumentsApi12MetadataCollectionName(null, null, (data, error) => {
            try {
              const displayE = 'collectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-putDocumentsApi12MetadataCollectionName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.putDocumentsApi12MetadataCollectionName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-putDocumentsApi12MetadataCollectionName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12MetadataCollectionName - errors', () => {
      it('should have a getDocumentsApi12MetadataCollectionName function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12MetadataCollectionName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectionName', (done) => {
        try {
          a.getDocumentsApi12MetadataCollectionName(null, (data, error) => {
            try {
              const displayE = 'collectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12MetadataCollectionName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12MetadataCollectionName - errors', () => {
      it('should have a deleteDocumentsApi12MetadataCollectionName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDocumentsApi12MetadataCollectionName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectionName', (done) => {
        try {
          a.deleteDocumentsApi12MetadataCollectionName(null, (data, error) => {
            try {
              const displayE = 'collectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12MetadataCollectionName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDocumentsApi12MetadataCollectionNameField - errors', () => {
      it('should have a putDocumentsApi12MetadataCollectionNameField function', (done) => {
        try {
          assert.equal(true, typeof a.putDocumentsApi12MetadataCollectionNameField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectionName', (done) => {
        try {
          a.putDocumentsApi12MetadataCollectionNameField(null, null, (data, error) => {
            try {
              const displayE = 'collectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-putDocumentsApi12MetadataCollectionNameField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.putDocumentsApi12MetadataCollectionNameField('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-putDocumentsApi12MetadataCollectionNameField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12MetadataSearchFields - errors', () => {
      it('should have a getDocumentsApi12MetadataSearchFields function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12MetadataSearchFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12MetadataSearchFields - errors', () => {
      it('should have a postDocumentsApi12MetadataSearchFields function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12MetadataSearchFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12MetadataSearchFields(null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12MetadataSearchFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12Metadata - errors', () => {
      it('should have a getDocumentsApi12Metadata function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12Metadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FoldersFolderIdMetadataCollectionName - errors', () => {
      it('should have a postDocumentsApi12FoldersFolderIdMetadataCollectionName function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FoldersFolderIdMetadataCollectionName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdMetadataCollectionName(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderIdMetadataCollectionName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectionName', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdMetadataCollectionName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'collectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderIdMetadataCollectionName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FoldersFolderIdMetadata - errors', () => {
      it('should have a postDocumentsApi12FoldersFolderIdMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FoldersFolderIdMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdMetadata(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderIdMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdMetadata('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderIdMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersFolderIdMetadata - errors', () => {
      it('should have a getDocumentsApi12FoldersFolderIdMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FoldersFolderIdMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdMetadata(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FoldersFolderIdMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12FoldersFolderIdMetadata - errors', () => {
      it('should have a deleteDocumentsApi12FoldersFolderIdMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDocumentsApi12FoldersFolderIdMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.deleteDocumentsApi12FoldersFolderIdMetadata(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12FoldersFolderIdMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.deleteDocumentsApi12FoldersFolderIdMetadata('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12FoldersFolderIdMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersFolderIdMetadataFields - errors', () => {
      it('should have a getDocumentsApi12FoldersFolderIdMetadataFields function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FoldersFolderIdMetadataFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdMetadataFields(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FoldersFolderIdMetadataFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FoldersFolderIdBulkCreate - errors', () => {
      it('should have a postDocumentsApi12FoldersFolderIdBulkCreate function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FoldersFolderIdBulkCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdBulkCreate(null, null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderIdBulkCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdBulkCreate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderIdBulkCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FoldersFolderIdDownload - errors', () => {
      it('should have a postDocumentsApi12FoldersFolderIdDownload function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FoldersFolderIdDownload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdDownload(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderIdDownload', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdDownload('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderIdDownload', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersFolderIdDownloadJobId - errors', () => {
      it('should have a getDocumentsApi12FoldersFolderIdDownloadJobId function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FoldersFolderIdDownloadJobId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdDownloadJobId(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FoldersFolderIdDownloadJobId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdDownloadJobId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FoldersFolderIdDownloadJobId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12FoldersFolderIdDownloadJobId - errors', () => {
      it('should have a deleteDocumentsApi12FoldersFolderIdDownloadJobId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDocumentsApi12FoldersFolderIdDownloadJobId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.deleteDocumentsApi12FoldersFolderIdDownloadJobId(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12FoldersFolderIdDownloadJobId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.deleteDocumentsApi12FoldersFolderIdDownloadJobId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12FoldersFolderIdDownloadJobId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersFolderIdDownloadJobIdPackage - errors', () => {
      it('should have a getDocumentsApi12FoldersFolderIdDownloadJobIdPackage function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FoldersFolderIdDownloadJobIdPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdDownloadJobIdPackage(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FoldersFolderIdDownloadJobIdPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdDownloadJobIdPackage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FoldersFolderIdDownloadJobIdPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersFolderId - errors', () => {
      it('should have a getDocumentsApi12FoldersFolderId function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FoldersFolderId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderId(null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FoldersFolderId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FoldersFolderId - errors', () => {
      it('should have a postDocumentsApi12FoldersFolderId function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FoldersFolderId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderId(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDocumentsApi12FoldersFolderId - errors', () => {
      it('should have a putDocumentsApi12FoldersFolderId function', (done) => {
        try {
          assert.equal(true, typeof a.putDocumentsApi12FoldersFolderId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.putDocumentsApi12FoldersFolderId(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-putDocumentsApi12FoldersFolderId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12FoldersFolderId - errors', () => {
      it('should have a deleteDocumentsApi12FoldersFolderId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDocumentsApi12FoldersFolderId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.deleteDocumentsApi12FoldersFolderId(null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12FoldersFolderId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FoldersFolderIdCopy - errors', () => {
      it('should have a postDocumentsApi12FoldersFolderIdCopy function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FoldersFolderIdCopy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdCopy(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderIdCopy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdCopy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderIdCopy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FoldersFolderIdMove - errors', () => {
      it('should have a postDocumentsApi12FoldersFolderIdMove function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FoldersFolderIdMove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdMove(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderIdMove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdMove('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderIdMove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersItems - errors', () => {
      it('should have a getDocumentsApi12FoldersItems function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FoldersItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersFolderIdItems - errors', () => {
      it('should have a getDocumentsApi12FoldersFolderIdItems function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FoldersFolderIdItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdItems(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FoldersFolderIdItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersSearchItems - errors', () => {
      it('should have a getDocumentsApi12FoldersSearchItems function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FoldersSearchItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fulltext', (done) => {
        try {
          a.getDocumentsApi12FoldersSearchItems(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fulltext is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FoldersSearchItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing querytext', (done) => {
        try {
          a.getDocumentsApi12FoldersSearchItems('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'querytext is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FoldersSearchItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersFolderIdSearchItems - errors', () => {
      it('should have a getDocumentsApi12FoldersFolderIdSearchItems function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FoldersFolderIdSearchItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdSearchItems(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FoldersFolderIdSearchItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fulltext', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdSearchItems('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fulltext is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FoldersFolderIdSearchItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing querytext', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdSearchItems('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'querytext is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FoldersFolderIdSearchItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersLists - errors', () => {
      it('should have a getDocumentsApi12FoldersLists function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FoldersLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idList', (done) => {
        try {
          a.getDocumentsApi12FoldersLists(null, null, (data, error) => {
            try {
              const displayE = 'idList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FoldersLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FoldersFolderIdTags - errors', () => {
      it('should have a getDocumentsApi12FoldersFolderIdTags function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FoldersFolderIdTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.getDocumentsApi12FoldersFolderIdTags(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FoldersFolderIdTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FoldersFolderIdTags - errors', () => {
      it('should have a postDocumentsApi12FoldersFolderIdTags function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FoldersFolderIdTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdTags(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderIdTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdTags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderIdTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDocumentsApi12FoldersFolderIdTags - errors', () => {
      it('should have a putDocumentsApi12FoldersFolderIdTags function', (done) => {
        try {
          assert.equal(true, typeof a.putDocumentsApi12FoldersFolderIdTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.putDocumentsApi12FoldersFolderIdTags(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-putDocumentsApi12FoldersFolderIdTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.putDocumentsApi12FoldersFolderIdTags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-putDocumentsApi12FoldersFolderIdTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12FoldersFolderIdTags - errors', () => {
      it('should have a deleteDocumentsApi12FoldersFolderIdTags function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDocumentsApi12FoldersFolderIdTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.deleteDocumentsApi12FoldersFolderIdTags(null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12FoldersFolderIdTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FoldersFolderIdConversation - errors', () => {
      it('should have a postDocumentsApi12FoldersFolderIdConversation function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FoldersFolderIdConversation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdConversation(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderIdConversation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12FoldersFolderIdConversation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FoldersFolderIdConversation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FilesData - errors', () => {
      it('should have a postDocumentsApi12FilesData function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FilesData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jsonInputParameters', (done) => {
        try {
          a.postDocumentsApi12FilesData(null, null, null, (data, error) => {
            try {
              const displayE = 'jsonInputParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing primaryFile', (done) => {
        try {
          a.postDocumentsApi12FilesData('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'primaryFile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FilesFileIdData - errors', () => {
      it('should have a postDocumentsApi12FilesFileIdData function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FilesFileIdData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdData(null, null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing primaryFile', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdData('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'primaryFile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdData - errors', () => {
      it('should have a getDocumentsApi12FilesFileIdData function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesFileIdData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdData(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileIdData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FilesFileIdMetadataCollectionName - errors', () => {
      it('should have a postDocumentsApi12FilesFileIdMetadataCollectionName function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FilesFileIdMetadataCollectionName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdMetadataCollectionName(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdMetadataCollectionName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectionName', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdMetadataCollectionName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'collectionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdMetadataCollectionName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FilesFileIdMetadata - errors', () => {
      it('should have a postDocumentsApi12FilesFileIdMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FilesFileIdMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdMetadata(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdMetadata('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdMetadata - errors', () => {
      it('should have a getDocumentsApi12FilesFileIdMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesFileIdMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdMetadata(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileIdMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12FilesFileIdMetadata - errors', () => {
      it('should have a deleteDocumentsApi12FilesFileIdMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDocumentsApi12FilesFileIdMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.deleteDocumentsApi12FilesFileIdMetadata(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12FilesFileIdMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.deleteDocumentsApi12FilesFileIdMetadata('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12FilesFileIdMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdMetadataFields - errors', () => {
      it('should have a getDocumentsApi12FilesFileIdMetadataFields function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesFileIdMetadataFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdMetadataFields(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileIdMetadataFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesLists - errors', () => {
      it('should have a getDocumentsApi12FilesLists function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idList', (done) => {
        try {
          a.getDocumentsApi12FilesLists(null, (data, error) => {
            try {
              const displayE = 'idList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileId - errors', () => {
      it('should have a getDocumentsApi12FilesFileId function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesFileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12FilesFileId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDocumentsApi12FilesFileId - errors', () => {
      it('should have a putDocumentsApi12FilesFileId function', (done) => {
        try {
          assert.equal(true, typeof a.putDocumentsApi12FilesFileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.putDocumentsApi12FilesFileId(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-putDocumentsApi12FilesFileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12FilesFileId - errors', () => {
      it('should have a deleteDocumentsApi12FilesFileId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDocumentsApi12FilesFileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.deleteDocumentsApi12FilesFileId(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12FilesFileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FilesFileIdCopy - errors', () => {
      it('should have a postDocumentsApi12FilesFileIdCopy function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FilesFileIdCopy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdCopy(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdCopy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdCopy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdCopy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FilesFileIdMove - errors', () => {
      it('should have a postDocumentsApi12FilesFileIdMove function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FilesFileIdMove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdMove(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdMove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdMove('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdMove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdVersions - errors', () => {
      it('should have a getDocumentsApi12FilesFileIdVersions function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesFileIdVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdVersions(null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileIdVersions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FilesFileIdReserve - errors', () => {
      it('should have a postDocumentsApi12FilesFileIdReserve function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FilesFileIdReserve === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdReserve(null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdReserve', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FilesFileIdUnreserve - errors', () => {
      it('should have a postDocumentsApi12FilesFileIdUnreserve function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FilesFileIdUnreserve === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdUnreserve(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdUnreserve', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdUnreserve('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdUnreserve', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdDataThumbnailImage - errors', () => {
      it('should have a getDocumentsApi12FilesFileIdDataThumbnailImage function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesFileIdDataThumbnailImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdDataThumbnailImage(null, null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileIdDataThumbnailImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdDataSmallImage - errors', () => {
      it('should have a getDocumentsApi12FilesFileIdDataSmallImage function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesFileIdDataSmallImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdDataSmallImage(null, null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileIdDataSmallImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdDataMediumImage - errors', () => {
      it('should have a getDocumentsApi12FilesFileIdDataMediumImage function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesFileIdDataMediumImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdDataMediumImage(null, null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileIdDataMediumImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdDataLargeImage - errors', () => {
      it('should have a getDocumentsApi12FilesFileIdDataLargeImage function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesFileIdDataLargeImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdDataLargeImage(null, null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileIdDataLargeImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdDataThumbnail - errors', () => {
      it('should have a getDocumentsApi12FilesFileIdDataThumbnail function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesFileIdDataThumbnail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdDataThumbnail(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileIdDataThumbnail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdDataRendition - errors', () => {
      it('should have a getDocumentsApi12FilesFileIdDataRendition function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesFileIdDataRendition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdDataRendition(null, null, null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileIdDataRendition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rendition', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdDataRendition('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'rendition is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileIdDataRendition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FilesFileIdDataRendition - errors', () => {
      it('should have a postDocumentsApi12FilesFileIdDataRendition function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FilesFileIdDataRendition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdDataRendition(null, null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdDataRendition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jsonInputParameters', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdDataRendition('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'jsonInputParameters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdDataRendition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing file', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdDataRendition('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'file is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdDataRendition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12FilesFileIdDataRendition - errors', () => {
      it('should have a deleteDocumentsApi12FilesFileIdDataRendition function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDocumentsApi12FilesFileIdDataRendition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.deleteDocumentsApi12FilesFileIdDataRendition(null, null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12FilesFileIdDataRendition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rendition', (done) => {
        try {
          a.deleteDocumentsApi12FilesFileIdDataRendition('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'rendition is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12FilesFileIdDataRendition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdDataRenditions - errors', () => {
      it('should have a getDocumentsApi12FilesFileIdDataRenditions function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesFileIdDataRenditions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdDataRenditions(null, null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileIdDataRenditions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdPages - errors', () => {
      it('should have a getDocumentsApi12FilesFileIdPages function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesFileIdPages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdPages(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileIdPages', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FilesFileIdPages - errors', () => {
      it('should have a postDocumentsApi12FilesFileIdPages function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FilesFileIdPages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdPages(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdPages', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdPreviewPath - errors', () => {
      it('should have a getDocumentsApi12FilesFileIdPreviewPath function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesFileIdPreviewPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdPreviewPath(null, null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileIdPreviewPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdTags - errors', () => {
      it('should have a getDocumentsApi12FilesFileIdTags function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesFileIdTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdTags(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileIdTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FilesFileIdTags - errors', () => {
      it('should have a postDocumentsApi12FilesFileIdTags function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FilesFileIdTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdTags(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdTags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDocumentsApi12FilesFileIdTags - errors', () => {
      it('should have a putDocumentsApi12FilesFileIdTags function', (done) => {
        try {
          assert.equal(true, typeof a.putDocumentsApi12FilesFileIdTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.putDocumentsApi12FilesFileIdTags(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-putDocumentsApi12FilesFileIdTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.putDocumentsApi12FilesFileIdTags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-putDocumentsApi12FilesFileIdTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12FilesFileIdTags - errors', () => {
      it('should have a deleteDocumentsApi12FilesFileIdTags function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDocumentsApi12FilesFileIdTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.deleteDocumentsApi12FilesFileIdTags(null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12FilesFileIdTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12FilesFileIdConversation - errors', () => {
      it('should have a postDocumentsApi12FilesFileIdConversation function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12FilesFileIdConversation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdConversation(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdConversation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12FilesFileIdConversation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12FilesFileIdConversation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12FilesFileIdAccesses - errors', () => {
      it('should have a getDocumentsApi12FilesFileIdAccesses function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12FilesFileIdAccesses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12FilesFileIdAccesses(null, null, null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12FilesFileIdAccesses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12PubliclinksFolderFolderId - errors', () => {
      it('should have a postDocumentsApi12PubliclinksFolderFolderId function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12PubliclinksFolderFolderId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.postDocumentsApi12PubliclinksFolderFolderId(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12PubliclinksFolderFolderId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12PubliclinksFolderFolderId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12PubliclinksFolderFolderId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12PubliclinksFolderFolderId - errors', () => {
      it('should have a getDocumentsApi12PubliclinksFolderFolderId function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12PubliclinksFolderFolderId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.getDocumentsApi12PubliclinksFolderFolderId(null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12PubliclinksFolderFolderId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12PubliclinksFileFileId - errors', () => {
      it('should have a postDocumentsApi12PubliclinksFileFileId function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12PubliclinksFileFileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.postDocumentsApi12PubliclinksFileFileId(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12PubliclinksFileFileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12PubliclinksFileFileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12PubliclinksFileFileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12PubliclinksFileFileId - errors', () => {
      it('should have a getDocumentsApi12PubliclinksFileFileId function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12PubliclinksFileFileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getDocumentsApi12PubliclinksFileFileId(null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12PubliclinksFileFileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12PubliclinksLinkId - errors', () => {
      it('should have a getDocumentsApi12PubliclinksLinkId function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12PubliclinksLinkId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.getDocumentsApi12PubliclinksLinkId(null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12PubliclinksLinkId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDocumentsApi12PubliclinksLinkId - errors', () => {
      it('should have a putDocumentsApi12PubliclinksLinkId function', (done) => {
        try {
          assert.equal(true, typeof a.putDocumentsApi12PubliclinksLinkId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.putDocumentsApi12PubliclinksLinkId(null, null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-putDocumentsApi12PubliclinksLinkId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.putDocumentsApi12PubliclinksLinkId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-putDocumentsApi12PubliclinksLinkId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12PubliclinksLinkId - errors', () => {
      it('should have a deleteDocumentsApi12PubliclinksLinkId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDocumentsApi12PubliclinksLinkId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.deleteDocumentsApi12PubliclinksLinkId(null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12PubliclinksLinkId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12Collections - errors', () => {
      it('should have a postDocumentsApi12Collections function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12Collections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12Collections(null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12Collections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12CollectionsCollectionId - errors', () => {
      it('should have a deleteDocumentsApi12CollectionsCollectionId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDocumentsApi12CollectionsCollectionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectionId', (done) => {
        try {
          a.deleteDocumentsApi12CollectionsCollectionId(null, (data, error) => {
            try {
              const displayE = 'collectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12CollectionsCollectionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12CollectionsCollectionIdFilesFileId - errors', () => {
      it('should have a postDocumentsApi12CollectionsCollectionIdFilesFileId function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12CollectionsCollectionIdFilesFileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectionId', (done) => {
        try {
          a.postDocumentsApi12CollectionsCollectionIdFilesFileId(null, null, (data, error) => {
            try {
              const displayE = 'collectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12CollectionsCollectionIdFilesFileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.postDocumentsApi12CollectionsCollectionIdFilesFileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12CollectionsCollectionIdFilesFileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12CollectionsCollectionIdFilesFileId - errors', () => {
      it('should have a deleteDocumentsApi12CollectionsCollectionIdFilesFileId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDocumentsApi12CollectionsCollectionIdFilesFileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectionId', (done) => {
        try {
          a.deleteDocumentsApi12CollectionsCollectionIdFilesFileId(null, null, (data, error) => {
            try {
              const displayE = 'collectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12CollectionsCollectionIdFilesFileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.deleteDocumentsApi12CollectionsCollectionIdFilesFileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12CollectionsCollectionIdFilesFileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12CollectionsCollectionIdFiles - errors', () => {
      it('should have a postDocumentsApi12CollectionsCollectionIdFiles function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12CollectionsCollectionIdFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectionId', (done) => {
        try {
          a.postDocumentsApi12CollectionsCollectionIdFiles(null, null, (data, error) => {
            try {
              const displayE = 'collectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12CollectionsCollectionIdFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12CollectionsCollectionIdFiles('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12CollectionsCollectionIdFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDocumentsApi12CollectionsCollectionIdFiles - errors', () => {
      it('should have a deleteDocumentsApi12CollectionsCollectionIdFiles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDocumentsApi12CollectionsCollectionIdFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectionId', (done) => {
        try {
          a.deleteDocumentsApi12CollectionsCollectionIdFiles(null, null, (data, error) => {
            try {
              const displayE = 'collectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12CollectionsCollectionIdFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.deleteDocumentsApi12CollectionsCollectionIdFiles('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-deleteDocumentsApi12CollectionsCollectionIdFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12CollectionsItems - errors', () => {
      it('should have a getDocumentsApi12CollectionsItems function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12CollectionsItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12ConfigCollaboration - errors', () => {
      it('should have a getDocumentsApi12ConfigCollaboration function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12ConfigCollaboration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12ConfigSitesDeliverycdn - errors', () => {
      it('should have a getDocumentsApi12ConfigSitesDeliverycdn function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12ConfigSitesDeliverycdn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12ConfigNotificationEmail - errors', () => {
      it('should have a getDocumentsApi12ConfigNotificationEmail function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12ConfigNotificationEmail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDocumentsApi12ConfigNotificationEmail - errors', () => {
      it('should have a putDocumentsApi12ConfigNotificationEmail function', (done) => {
        try {
          assert.equal(true, typeof a.putDocumentsApi12ConfigNotificationEmail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.putDocumentsApi12ConfigNotificationEmail(null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-putDocumentsApi12ConfigNotificationEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12SitesSiteIdSite - errors', () => {
      it('should have a postDocumentsApi12SitesSiteIdSite function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12SitesSiteIdSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.postDocumentsApi12SitesSiteIdSite(null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12SitesSiteIdSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12SitesSiteIdSite('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12SitesSiteIdSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12TemplatesTemplateIdSite - errors', () => {
      it('should have a postDocumentsApi12TemplatesTemplateIdSite function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12TemplatesTemplateIdSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.postDocumentsApi12TemplatesTemplateIdSite(null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12TemplatesTemplateIdSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12TemplatesTemplateIdSite('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12TemplatesTemplateIdSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12ApplinksFolderFolderId - errors', () => {
      it('should have a postDocumentsApi12ApplinksFolderFolderId function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12ApplinksFolderFolderId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing folderId', (done) => {
        try {
          a.postDocumentsApi12ApplinksFolderFolderId(null, null, (data, error) => {
            try {
              const displayE = 'folderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12ApplinksFolderFolderId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12ApplinksFolderFolderId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12ApplinksFolderFolderId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12ApplinksFileFileId - errors', () => {
      it('should have a postDocumentsApi12ApplinksFileFileId function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12ApplinksFileFileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.postDocumentsApi12ApplinksFileFileId(null, null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12ApplinksFileFileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payload', (done) => {
        try {
          a.postDocumentsApi12ApplinksFileFileId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payload is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12ApplinksFileFileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDocumentsApi12ApplinksToken - errors', () => {
      it('should have a putDocumentsApi12ApplinksToken function', (done) => {
        try {
          assert.equal(true, typeof a.putDocumentsApi12ApplinksToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi - errors', () => {
      it('should have a getDocumentsApi function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersion - errors', () => {
      it('should have a getDocumentsApiVersion function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApiVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.getDocumentsApiVersion(null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApiVersion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalog - errors', () => {
      it('should have a getDocumentsApiVersionMetadataCatalog function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApiVersionMetadataCatalog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalog(null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApiVersionMetadataCatalog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogFolders - errors', () => {
      it('should have a getDocumentsApiVersionMetadataCatalogFolders function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApiVersionMetadataCatalogFolders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogFolders(null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApiVersionMetadataCatalogFolders', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogFiles - errors', () => {
      it('should have a getDocumentsApiVersionMetadataCatalogFiles function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApiVersionMetadataCatalogFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogFiles(null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApiVersionMetadataCatalogFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogUsers - errors', () => {
      it('should have a getDocumentsApiVersionMetadataCatalogUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApiVersionMetadataCatalogUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogUsers(null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApiVersionMetadataCatalogUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogApplinks - errors', () => {
      it('should have a getDocumentsApiVersionMetadataCatalogApplinks function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApiVersionMetadataCatalogApplinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogApplinks(null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApiVersionMetadataCatalogApplinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogShares - errors', () => {
      it('should have a getDocumentsApiVersionMetadataCatalogShares function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApiVersionMetadataCatalogShares === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogShares(null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApiVersionMetadataCatalogShares', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogPubliclinks - errors', () => {
      it('should have a getDocumentsApiVersionMetadataCatalogPubliclinks function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApiVersionMetadataCatalogPubliclinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogPubliclinks(null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApiVersionMetadataCatalogPubliclinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogMetadata - errors', () => {
      it('should have a getDocumentsApiVersionMetadataCatalogMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApiVersionMetadataCatalogMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogMetadata(null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApiVersionMetadataCatalogMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogConfiguration - errors', () => {
      it('should have a getDocumentsApiVersionMetadataCatalogConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApiVersionMetadataCatalogConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogConfiguration(null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApiVersionMetadataCatalogConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogCollections - errors', () => {
      it('should have a getDocumentsApiVersionMetadataCatalogCollections function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApiVersionMetadataCatalogCollections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogCollections(null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApiVersionMetadataCatalogCollections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogSites - errors', () => {
      it('should have a getDocumentsApiVersionMetadataCatalogSites function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApiVersionMetadataCatalogSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogSites(null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApiVersionMetadataCatalogSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApiVersionMetadataCatalogTemplates - errors', () => {
      it('should have a getDocumentsApiVersionMetadataCatalogTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApiVersionMetadataCatalogTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.getDocumentsApiVersionMetadataCatalogTemplates(null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApiVersionMetadataCatalogTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12UsersItems - errors', () => {
      it('should have a getDocumentsApi12UsersItems function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12UsersItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing info', (done) => {
        try {
          a.getDocumentsApi12UsersItems(null, (data, error) => {
            try {
              const displayE = 'info is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12UsersItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDocumentsApi12UsersSearchItems - errors', () => {
      it('should have a getDocumentsApi12UsersSearchItems function', (done) => {
        try {
          assert.equal(true, typeof a.getDocumentsApi12UsersSearchItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing email', (done) => {
        try {
          a.getDocumentsApi12UsersSearchItems(null, (data, error) => {
            try {
              const displayE = 'email is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-getDocumentsApi12UsersSearchItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDocumentsApi12UsersUserIDTransferContent - errors', () => {
      it('should have a postDocumentsApi12UsersUserIDTransferContent function', (done) => {
        try {
          assert.equal(true, typeof a.postDocumentsApi12UsersUserIDTransferContent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userID', (done) => {
        try {
          a.postDocumentsApi12UsersUserIDTransferContent(null, null, (data, error) => {
            try {
              const displayE = 'userID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12UsersUserIDTransferContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetUserID', (done) => {
        try {
          a.postDocumentsApi12UsersUserIDTransferContent('fakeparam', null, (data, error) => {
            try {
              const displayE = 'targetUserID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-oracle_cloud-adapter-postDocumentsApi12UsersUserIDTransferContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
