
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:13PM

See merge request itentialopensource/adapters/adapter-oracle_cloud!13

---

## 0.4.3 [09-14-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-oracle_cloud!11

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:23PM

See merge request itentialopensource/adapters/adapter-oracle_cloud!10

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:36PM

See merge request itentialopensource/adapters/adapter-oracle_cloud!9

---

## 0.4.0 [07-08-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-oracle_cloud!8

---

## 0.3.3 [03-28-2024]

* Changes made at 2024.03.28_13:17PM

See merge request itentialopensource/adapters/cloud/adapter-oracle_cloud!7

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_15:32PM

See merge request itentialopensource/adapters/cloud/adapter-oracle_cloud!6

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:44AM

See merge request itentialopensource/adapters/cloud/adapter-oracle_cloud!5

---

## 0.3.0 [12-27-2023]

* Migration from Auto Branch

See merge request itentialopensource/adapters/cloud/adapter-oracle_cloud!4

---

## 0.2.0 [05-20-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-oracle_cloud!1

---

## 0.1.1 [12-15-2021]

- Initial Commit

See commit 7d8f29b

---
