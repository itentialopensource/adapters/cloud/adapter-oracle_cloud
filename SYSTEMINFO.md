# Oracle Cloud

Vendor: Oracle Cloud
Homepage: https://www.oracle.com/

Product: Oracle Cloud
Product Page: https://www.oracle.com/cloud/

## Introduction
We classify Oracle Cloud into the Cloud domain as Oracle Cloud provides the tools and services to build, deploy, integrate, and manage applications and infrastructure in the cloud efficiently and securely. The adapter currently contains API's related to the Content Management service.

## Why Integrate
The Oracle Cloud adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Oracle Cloud. With this adapter you have the ability to perform operations on items such as:

- Templates
- Configurations
- Folders
- Files
- Sites
- Shares

## Additional Product Documentation
[Oracle Cloud API Documentation](https://docs.oracle.com/en/cloud/paas/content-cloud/rest-api-documents/rest-endpoints.html)
[Oracle Cloud Authentication Information](https://docs.oracle.com/en/cloud/paas/content-cloud/rest-api-documents/Authorization.html)
[Oracle Content Management](https://docs.oracle.com/en/cloud/paas/content-cloud/)